FROM ubuntu:16.04

MAINTAINER Xinyaun Yao <yao.xinyuan@canon.co.jp>

RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    bash-completion \
    openssh-server \
    build-essential \
    htop \
    tmux \
    curl \
    apt-transport-https \
    ca-certificates \
    curl \
    tree \
    software-properties-common 

RUN add-apt-repository ppa:jonathonf/vim
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
RUN chmod +x /usr/local/bin/docker-compose
RUN apt-key fingerprint 0EBFCD88 && \
    add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"

RUN apt-get update && apt-get install -y --no-install-recommends \
    docker-ce \
    vim-nox 

RUN echo  StrictHostKeyChecking no >> /etc/ssh/ssh_config

RUN git clone https://github.com/h4nyu/.vim.git /root/.vim 
VOLUME /root

COPY ./entrypoint.sh /usr/local/bin/
RUN ln -s usr/local/bin/entrypoint.sh /
ENTRYPOINT ["entrypoint.sh"]
WORKDIR /root

